const express = require("express");
const axios = require("axios");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());

const handleEvent = async (type, data) => {
  const { id, content, postId } = data;
  if (type === "CommentCreated") {
    const status = content.includes("carrot") ? "rejected" : "approved";

    await axios.post("http://localhost:4005/events", {
      type: "CommentModerated",
      data: {
        id,
        content,
        postId,
        status,
      },
    });
  }
};

app.post("/events", async (req, res) => {
  const { type, data } = req.body;

  await handleEvent(type, data);

  res.send({});
});

app.listen(4003, async () => {
  console.log("Listening on 4003");

  const { data } = await axios.get("http://localhost:4005/events");

  for (let event of data) {
    console.log("Processing event:", event.type);

    await handleEvent(event.type, event.data);
  }
});
